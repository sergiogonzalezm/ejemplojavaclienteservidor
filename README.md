# Ejemplo Java cliente-servidor

En este ejemplo se muestra una arquitectura cliente-servidor donde un cliente pide un fichero al servidor y este se lo devuelve.

Las comunicaciones son siempres seguras y además se comprueba que el fichero llego correctamente al terminar el envío.

