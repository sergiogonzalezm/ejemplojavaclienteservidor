package cliente;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.util.Base64;

import javax.crypto.spec.SecretKeySpec;

import cifrado.CifradoAes;
import cifrado.CifradoRsa;
import cifrado.Sha256;

public class Cliente {
	
	private static final String IP_SERVIDOR = "127.0.0.1";
	private static final int PUERTO = 6000;
	private static final String CODIFICACION = "UTF-8";
	private static final String SHA = "SHA-256";
	private static final Character CARACTER_SENALIZACION_LINEA = '¿';
	
	private static Socket cliente = null;
	private static BufferedReader lecturaServidor;
	private static PrintWriter escrituraServidor;
	private static PrintWriter escrituraFichero;
	private static BufferedReader lecturaConsola;
    
	public static void main(String[] arg) {
		KeyPair clavesAsimetricas;
		SecretKeySpec claveSimetrica;
		String ficheroPeticion;
		File fichero;
		String lineaActual;
		MessageDigest calculadorSha;
		
		try {
			// Obtengo el objeto que me permite calcular un hash.
			calculadorSha = MessageDigest.getInstance(SHA);
			
			
			System.out.println("PROGRAMA CLIENTE INICIADO....");
			// Me conecto al servidor y genero dos objetos para leer de él y escribirle
			cliente = new Socket(IP_SERVIDOR, PUERTO);
	       
			escrituraServidor = new PrintWriter(
					new BufferedWriter(
							new OutputStreamWriter(
									cliente.getOutputStream(), CODIFICACION
							)
					), true
			);
			
			lecturaServidor = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
	
			System.out.println("Conexión con el servidor iniciada");
			
			// Se generan las claves asimétricas y se envía la pública
			clavesAsimetricas = CifradoRsa.generarClavesRsa();
			String publicKeyBase64 = Base64.getEncoder().encodeToString(clavesAsimetricas.getPublic().getEncoded());
			escrituraServidor.println(publicKeyBase64);
			
			System.out.println("CLAVE ASIMETRICA ENVIADA");
			
			// Recibo la clave simétrica del servidor, la cual este me ha encriptado con mi clave pública
			claveSimetrica = CifradoAes.obtenerClaveTransparente(CifradoRsa.desencriptar(lecturaServidor.readLine(), clavesAsimetricas.getPrivate()));
			System.out.println("Clave simetrica recibida correctamente");
			
			// A partir de este momento todo esta cifrado con clave simétrica
			// Pregunto al usuario que fichero quiere recibir del servidor
			System.out.print("¿Qué fichero desea obtener del servidor?: ");
			lecturaConsola = new BufferedReader(new InputStreamReader(System.in));
			ficheroPeticion = lecturaConsola.readLine();
			
			// Pido el fichero al servidor
			escrituraServidor.println(CifradoAes.encriptar(ficheroPeticion, claveSimetrica));
			System.out.println("Petición del fichero enviada");
			
			
			// Controlo los posibles casos de uso (el servidor ha enviado o no el fichero)
			lineaActual = CifradoAes.desencriptar(lecturaServidor.readLine(), claveSimetrica);
			if (lineaActual.length() <= 1)
				// Si la primera línea tiene un solo carácter significa que el servidor no envía el fichero.
				System.out.println("El fichero solicitado al servidor no existe o no esta disponible");
			else {
				// Creo el fichero y controlo nuevos casos de uso (el fichero existe o no existe)
				fichero = new File(ficheroPeticion);
				if (fichero.exists())
					System.out.println("No se pudo guardar el fichero porque tiene otro con el mismo nombre");
				else {
					fichero.createNewFile();
					escrituraFichero = new PrintWriter(
							new BufferedWriter(
									new FileWriter(
											fichero
									)
							), true
					);
					
					
					// Guardo el fichero
					while (lineaActual.charAt(0) == CARACTER_SENALIZACION_LINEA) {
						escrituraFichero.println(lineaActual.substring(1, lineaActual.length()));
						// Mientras voy actualizando el hash
						calculadorSha.update(lineaActual.substring(1, lineaActual.length()).getBytes());
						lineaActual = CifradoAes.desencriptar(lecturaServidor.readLine(), claveSimetrica);
					}
					
					//System.out.println("SHA-256 del cliente = " + sha256ToString(calculadorSha.digest()));
					
					// Una vez tengo el fichero el servidor me envía el código hash (el cual sera la última línea que he recibido)
					if (Sha256.sha256ToString(calculadorSha.digest()).equals(lineaActual))
						System.out.println("El fichero se recibio correctamente");
					else
						System.out.println("El fichero recibido tiene algún fallo");
				}
			}
		} catch (IOException e) {
			System.out.println("Hubo un error de comunicación con el cliente");
		} catch (Exception e) {
			System.out.println("Hubo un error inesperado");
		} finally {
			try {
				if (escrituraFichero != null)
					escrituraFichero.close();
				if (cliente != null)
					cliente.close();
				if (lecturaConsola != null)
					lecturaConsola.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
