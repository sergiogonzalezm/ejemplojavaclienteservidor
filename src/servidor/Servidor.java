package servidor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.PublicKey;

import javax.crypto.spec.SecretKeySpec;

import cifrado.CifradoAes;
import cifrado.CifradoRsa;
import cifrado.Sha256;

public class Servidor {
	
	private static final int PUERTO = 6000;
	private static final String CODIFICACION = "UTF-8";
	private static final String SHA = "SHA-256";
	private static final String STRING_CLAVE_SIMETRICA = "No te olvides de poner el Where en el DELETE FROM";
	private static final Character CARACTER_SENALIZACION_LINEA = '¿';
	
	private static ServerSocket servidor = null;
	private static Socket cliente;
	private static BufferedReader lecturaCliente;
	private static PrintWriter escrituraCliente;
	private static File fichero;
	private static BufferedReader lecturaFichero;
	
	public static void main(String[] arg) {
		PublicKey clavePublicaCliente;
		SecretKeySpec claveSimetrica;
		String ficheroPeticion;
		String lineaActual;
		MessageDigest calculadorSha;
		
		try {
			// Obtengo una clave simétrica
			claveSimetrica = CifradoAes.obtenerClaveTransparente(STRING_CLAVE_SIMETRICA);
			// Obtengo el objeto que me permite calcular un hash.
			calculadorSha = MessageDigest.getInstance(SHA);
			
			
			// Espero al cliente
			servidor = new ServerSocket(PUERTO);
	        System.out.println("Esperando al cliente.....");
	        cliente = servidor.accept();
	        
	        // Una vez tengo al cliente genero dos objetos (uno para leer de él y otro para escribirle)
	        escrituraCliente = new PrintWriter(
					new BufferedWriter(
							new OutputStreamWriter(
									cliente.getOutputStream(), CODIFICACION
							)
					), true
			);
			
			
			lecturaCliente = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
	        
	        // Recogo la clave asimétrica pública del cliente
			clavePublicaCliente = CifradoRsa.generarClavePublica(lecturaCliente.readLine());
			
	        System.out.println("CLAVE PUBLICA RECIBIDA");
	        
	        // Envío al cliente la clave simétrica para la comunicación cifrada con su clave asimétrica pública.
	        escrituraCliente.println(CifradoRsa.encriptar(STRING_CLAVE_SIMETRICA, clavePublicaCliente));
	        
	        System.out.println("Enviada clave simetrica");
	        
	        // A partir de este momento todo esta cifrado con clave simétrica
	        // Leo que fichero del servidor quiere el cliente.
	        ficheroPeticion = CifradoAes.desencriptar(lecturaCliente.readLine(), claveSimetrica);
	        System.out.println("El cliente ha pedido el fichero " + ficheroPeticion);
	        
	        
	        // Controlo los posibles casos de uso (el fichero existe o no existe)
	        fichero = new File(ficheroPeticion);
	        if (!fichero.exists())
	        	// Si el cliente recibe solo ese carácter entendera que el fichero no existe
	        	escrituraCliente.println(CifradoAes.encriptar(CARACTER_SENALIZACION_LINEA.toString(), claveSimetrica));
	        else {
	        	lecturaFichero = new BufferedReader(new FileReader(fichero));
	        	calculadorSha.reset();
	        	
	        	
	        	lineaActual = lecturaFichero.readLine();
	        	while (lineaActual != null) {
	        		// Envío las líneas del ficheros precedidas de un carácter que el cliente conoce.
	        		// Cuando el cliente deje de tener ese carácter en primer lugar sabra que el fichero no tiene más líneas.
	        		escrituraCliente.println(CifradoAes.encriptar(CARACTER_SENALIZACION_LINEA + lineaActual, claveSimetrica));
	        		// Voy calculando el hash del fichero leído mientras lo leo.
	        		calculadorSha.update(lineaActual.getBytes());
	        		lineaActual = lecturaFichero.readLine();
	        	}
	        	//System.out.println("SHA-256 del servidor = " + sha256ToString(calculadorSha.digest()));
	        	
	        	// Envío el hash del fichero al cliente, así este podrá saber si lo recibió bien o no
	        	escrituraCliente.println(CifradoAes.encriptar(Sha256.sha256ToString(calculadorSha.digest()), claveSimetrica));
	        }
		} catch (IOException e) {
			System.out.println("Hubo un error de comunicación con el cliente");
		} catch (Exception e) {
			System.out.println("Hubo un error inesperado");
		} finally {
			try {
				if (lecturaFichero != null)
					lecturaFichero.close();
				if (cliente != null)
					cliente.close();
				if (servidor != null)
					servidor.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
