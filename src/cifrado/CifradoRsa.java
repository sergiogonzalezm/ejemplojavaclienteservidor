package cifrado;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

public final class CifradoRsa {
	// No se pueden generar claves mayores que 16384
	private static final int LONGITUD = 2048;
	private static final String ALG = "RSA";
	
	private CifradoRsa() {}
	
	// Permite encriptar un String
	public static String encriptar(String mensaje, Key clave) throws Exception {
		Cipher rsaCifrado = Cipher.getInstance("RSA");
		rsaCifrado.init(Cipher.ENCRYPT_MODE, clave);
		byte[] criptogramaBytes = Base64.getEncoder().encode(rsaCifrado.doFinal(mensaje.getBytes("utf8")));
		
		return new String(criptogramaBytes);
	}
	
	// Permite desencriptar un String
	public static String desencriptar(String criptograma, Key clave) throws Exception {
		Cipher rsaCifrado = Cipher.getInstance(ALG);
		rsaCifrado.init(Cipher.DECRYPT_MODE, clave);
		byte[] mensajeBytes = Base64.getDecoder().decode(criptograma.getBytes("utf8"));
		
		return new String(rsaCifrado.doFinal(mensajeBytes));
	}
	
	// Genera un par de claves asimétricas
	public static KeyPair generarClavesRsa() throws NoSuchAlgorithmException {
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALG);
		keyGen.initialize(LONGITUD);
		return keyGen.generateKeyPair();
	}
	
	// Genera la clave asimética pública a partir de su String
	public static PublicKey generarClavePublica(String clave) throws NoSuchAlgorithmException, InvalidKeySpecException {
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(clave));
		return keyFactory.generatePublic(keySpec);
	}
}
