package cifrado;

public class Sha256 {
	
	private Sha256() {}
	
	// Crea un String a partir de un SHA256
	public static String sha256ToString(byte[] hash) {
		StringBuffer sb = new StringBuffer();
	    
		for(byte b : hash) {        
			sb.append(String.format("%02x", b));
		}
		    
		return sb.toString();
	}
}
