package cifrado;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public final class CifradoAes {
	private static final String CIFRADO = "AES";
	private static final String CODIFICACION = "UTF-8";
	private static final String SHA = "SHA-256";
	
	private CifradoAes() {}
	
	// Obtiene una clave simética transparente
	public static SecretKeySpec obtenerClaveTransparente(String miClave) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		// Se puede poner con o sin guión y mayúscula o minúscula.
		byte[] miClaveEnBytes = miClave.getBytes(CODIFICACION);
		
		// El SHA debe ser siempre un múltiplo de 16.
		MessageDigest sha = MessageDigest.getInstance(SHA);
		miClaveEnBytes = sha.digest(miClaveEnBytes);
		// Solo recogemos los 16 primeros bytes del hash generado
		byte[] miClaveSha2 = Arrays.copyOf(miClaveEnBytes, 16);
		
		return new SecretKeySpec(miClaveSha2, CIFRADO);
	}
	
	// Permite encriptar un String
	public static String encriptar(String mensaje, SecretKey clave) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
		Cipher c = Cipher.getInstance(CIFRADO);
		c.init(Cipher.ENCRYPT_MODE, clave);
		// Se puede poner con o sin guión y mayúscula o minúscula.
		byte[] encVal = c.doFinal(mensaje.getBytes(CODIFICACION));
		
		// Hay que usar SIEMPRE un múltiplo de 64.
		// En versiones anteriores a Java 7 había que usar la librería externa Commons-codec.
		return Base64.getEncoder().encodeToString(encVal);
	}
	
	// Permite desencriptar un String
	public static String desencriptar(String criptograma, SecretKey clave) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher c = Cipher.getInstance(CIFRADO);
		c.init(Cipher.DECRYPT_MODE, clave);
		byte[] decValue = Base64.getDecoder().decode(criptograma);
		byte[] decryptedVal = c.doFinal(decValue);
				
		return new String(decryptedVal);
	}
}
